��          �      �       H     I  =  J  f  �     �     �  
     m        �     �     �     �  
   �  d   �  k  %  �   �  	  `  �  j	               :  o   F     �     �     �     �  	   �  �   �                            
                          	           
                            These tools are used to collect statistics about user behaviour that helps us to improve our
                            website and services.
                            No personal data are collected.
                         
                        Websites do not have full control over cookies that may set by various third-party scripts.
                        To see detailed information about and to manage cookies in your browser, please check its
                        privacy
                        settings.
                     
            <a href="#" class="btn btn-default btn-xs js-cookie-accept pull-right">Accept</a>
            This website uses cookies to help ensure that you enjoy the best experience.<br/>
            If you do not consent to our use of cookies, please adjust your
            <a href="#" class="js-cookie-settings">privacy settings</a> accordingly.
         Accept Currently none in use. Essentials In order to improve your experience of our services, we will customise your experience based upon your usage. Name Ordering Personalisation Privacy Settings Statistics These cookies and scripts cannot be deactivated as they are needed to correctly render this website. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-08-16 16:06+0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
Diese Tools werden verwendet, um Statistiken über das Nutzerverhalten zu sammeln, die uns helfen, unsere Website und unsere Dienstleistungen zu verbessern. Es werden keine personenbezogenen Daten erhoben. 
Websites haben nicht die volle Kontrolle über Cookies, die von verschiedenen Skripten Dritter gesetzt werden können. Um detaillierte Informationen über die Verwaltung von Cookies in Ihrem Browser zu erhalten, überprüfen Sie bitte die Datenschutzeinstellungen. 
            <a href="#" class="btn btn-default btn-xs js-cookie-accept pull-right">Akzeptieren</a>
            Diese Website verwendet Cookies, um sicherzustellen, dass Sie das beste Erlebnis geniessen können.<br/>
            Wenn Sie mit der Verwendung von Cookies nicht einverstanden sind, passen Sie bitte Ihre
            <a href="#" class="js-cookie-settings">Datenschutzeinstellungen</a> entsprechend an.
         Akzeptieren Derzeit nicht in Verwendung. Wesentliche Um Ihre Erfahrung mit unseren Services zu verbessern, passen wir Ihre Erfahrung basierend auf Ihrer Nutzung an. Name Reihenfolge Personalisierung Privatsphäre-Einstellungen Statistik Diese Cookies und Skripte können nicht deaktiviert werden, da sie für die korrekte Darstellung dieser Website benötigt werden. 